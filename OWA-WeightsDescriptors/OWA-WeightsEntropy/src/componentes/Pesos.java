package componentes;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Pesos {
	
	public static final double IMPRECISION = 1e-6f;
	private ArrayList<Peso> map ;
	
	public Pesos(){
		map = new ArrayList<Peso>();
	}
    
    public int size() {
    	return map.size();
    }
    
    public double getSum() {
	    double sum = recompute();
		return sum;
    }
    
    public void putWeight(Peso weight)
    {
		if (weight != null){
			map.add(weight);
		}
    }
    
    public Peso[] getWeights() {

	    Peso[] weight = new Peso[map.size()];
	    int i = 0;
	    for( i=0;i<map.size();i++  ) {
	    	weight[i] = map.get(i);
	    }
		return weight;
    }
    
    public double[] getWeightsValues() {
	    double[] weight = new double[map.size()];
	    int i = 0;
	    for( i=0;i<map.size();i++  ) {
	    	weight[i] = map.get(i).getValue();
	    }
		return weight;
    }
    
    public boolean isNormalized()
    {
	    double sum = recompute();
		final boolean sumToOne = Math.abs(sum - 1f) < IMPRECISION;
		return sumToOne;
    }

    private double recompute()
    {
		double sum = 0;
		for (final Peso weight : getWeights()) {
		    final double value = weight.getValue();
		    sum = sum + value;
		}
		return sum;
    }
	
    @Override
    public boolean equals(final Object obj)
    {
		if (this == obj) {
		    return true;
		}
		if (obj == null) {
		    return false;
		}
		if (getClass() != obj.getClass()) {
		    return false;
		}
		final Pesos other = (Pesos) obj;
		if (map == null) {
		    if (other.map != null) {
			return false;
		    }
		} else if (!(map).equals(other.map)) {
		    return false;
		}
		return true;
    }

    @Override
    public int hashCode() 
    {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((map == null) ? 0 : map.hashCode());
		return result;
    }
    
    @Override
    public String toString()
    {
		return "Weights " + toString(map, map.size());
    }

    private String toString(Collection<?> collection, int maxLen)
    {
		final StringBuilder builder = new StringBuilder();
		builder.append("[");
		int i = 0;
		for (final Iterator<?> iterator = collection.iterator(); iterator.hasNext() && i < maxLen; i++) {
		    if (i > 0) {
			builder.append(", ");
		    }
		    builder.append(iterator.next());
		}
		builder.append("]");
		return builder.toString();
    }
}
