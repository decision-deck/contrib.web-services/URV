package owa;

import java.util.HashMap;

//import componentes.Alternativa;
import componentes.Situation;

/**
 * 
 * @author Jonatan Moreno and Aida Valls
 * Class to compute the OWA-related descriptive measures about the set of weights
 * 	orness
 * 	balance
 * 	divergence
 * 	entropy
 */

public class owaMeasures {
	double orness;
	double entropy;
	double balance;
	double divergence;
	Situation situation;
	int n;
	double[] weightValues;
	HashMap<String, Double> measures;
	
	public owaMeasures(Situation situation) {
		this.situation = situation;
		this.n = situation.getWeights().size();
		this.weightValues = situation.getWeights().getWeightsValues();
		this.measures = new HashMap<String, Double>();
	}
	
	/**
     * Function that calculate the orness based on the weights
     * @return orness value
     */
    public void orness() {
    	int j=0;
    	double sum = 0;
    	double numerador;
    	double denominador;
    	double fraccio;
    	for (j=0; j< this.n; j++) {
    		numerador = (double)(this.n-(j+1));
    		denominador = (double) (this.n-1);
    		fraccio = numerador/denominador;
    		sum = sum + this.weightValues[j]*fraccio;
    	}
    	this.orness = sum;
    }
    
    /**
     * Function that computes the entropy from the weights
     * @return entropy value
     */
    public void entropy() {
    	int j;
    	double sum = 0;
    	double aux;
    	for (j=0; j<this.n; j++) {
    		aux = this.weightValues[j];
    		if (aux !=0) {
    			sum =sum + this.weightValues[j]*Math.log(this.weightValues[j]);
    		}
    	}
    	this.entropy = -sum;
    }

    /**
     * Function that computes the balance from the weights
     * @return balance value
     */
	public void balance() {
		int j;
    	double sum = 0;
    	double numerador;
    	double denominador;
    	double fraccio;
    	for (j=0; j<this.n; j++) {
    		numerador = this.n+1-2*(j+1);
    		denominador = this.n-1;
    		fraccio = numerador/denominador;
    		sum = sum + (this.weightValues[j]*fraccio);
    	}
    	this.balance = sum;
	}
	
	/**
	 * Function that computes the divergence from te weights
	 * @return divergence value
	 */
	public void divergence() {
		int j;
    	double sum = 0;
    	double numerador;
    	double denominador;
    	double fraccio;
    	double quadrat;
    	for (j=0; j<this.n; j++) {
    		numerador = this.n - (j+1);
    		denominador = this.n-1;
    		fraccio = numerador/denominador;
    		quadrat = (fraccio-this.orness)*(fraccio-this.orness);
    		sum= sum + (this.weightValues[j]*quadrat);    		
    	}
    	this.divergence = sum;
	}

	public double getOrness() {
		return orness;
	}

	public void setOrness(double orness) {
		this.orness = orness;
	}

	public double getEntropy() {
		return entropy;
	}

	public void setEntropy(double entropy) {
		this.entropy = entropy;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getDivergence() {
		return divergence;
	}

	public void setDivergence(double divergence) {
		this.divergence = divergence;
	}

	public HashMap<String, Double> calculated() {
		this.measures.put("Orness", this.orness);
		this.measures.put("Entropy", this.entropy);
		this.measures.put("Balance", this.balance);
		this.measures.put("Divergence", this.divergence);
		
		return measures;
	}
	
	
}
