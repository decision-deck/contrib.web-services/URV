package main;

/**
 * 
 * <strong>Description measures for teh OWA weights: Entropy)</strong>
 * 
 * @authors Aida Valls, Jonathan Moreno.
 * Universitat Rovira i Virgili (URV), Tarragona, Spain
 * Escola Tecnica Superior d'Enginyeria, Departament d'Enginyeria Informatica i Matematiques.
 * 
 * @contact Aida Valls <aida.valls@urv.cat>
 * 
 * @version 1.0
 * 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlException;
import org.decisionDeck.x2012.xmcda220.XMCDADocument;
import componentes.Situation;
import owa.owaMeasures;

public class Main {
	
	private final static String OUTMEASURESFILE = "/owaMeasures.xml";
	private final static String OUTMESSAGEFILE = "/messages.xml";
	private static boolean error = false;
	private static String input = null;
	private static String output = null;
	private static XMCDADocument docMessage;
	private static Situation situation;
	
	private static void parseComannd (String args []) throws ParseException{
		
		/*Basic structure*/
		CommandLineParser parser = new BasicParser();
		
		/*Prepare the options*/
		Options options = new Options();
		options.addOption("i", true, "Input folder");
		options.addOption("o", true, "Output folder");
		options.addOption("h", "help", false, "Show help");
		
		/*Read the information*/
		CommandLine cmdLine = parser.parse(options, args);

		if (cmdLine.hasOption("h")){
			new HelpFormatter().printHelp(owaMeasures.class.getCanonicalName(), options );
		}

		input = cmdLine.getOptionValue("i");
		if(input == null){
			error = true;
			docMessage = MakeFile.addMessageToLog(docMessage, "Fail input folder",true);
		}
		
		output = cmdLine.getOptionValue("o");
		if (output == null){
			error = true;
			docMessage = MakeFile.addMessageToLog(docMessage, "Fail output folder",true);
		}
	}
	
	public static void main(String args []) {
		
		try {
			/*Prepare the Message document*/
			docMessage = MakeFile.makeMessageDocument();
			
			/*Parse the command line*/
			parseComannd(args);
			
			File dir = new File(input);
			File[] listFile = dir.listFiles();
			if (listFile.length == 0) {
				docMessage = MakeFile.addMessageToLog(docMessage, "No files in the directory specified",true);
			} else {
				ArrayList<XmlError> errorList = new ArrayList<XmlError>();
				situation = ReadXMCDA.readData(listFile,errorList);
				
				if ( errorList.size() > 0 || error )
				/**
				 * If the software detects any error on format of file or in the parameters from command line
				 * it execute the firts part,
				 * and it don't calculat the owa Method, and it don't create the
				 * document by owa results (it's logical).  
				 */
				{
					for (int i = 0; i < errorList.size(); i++)
					{
						XmlError xmlerror = (XmlError)errorList.get(i);
						docMessage = MakeFile.addMessageToLog(docMessage,"Error: "+xmlerror.getMessage(),true);
						docMessage = MakeFile.addMessageToLog(docMessage, "Locate Error: "+xmlerror.getCursorLocation().xmlText(),true);
					}
				} else {
					if(situation.validationSituation()){
						
						owaMeasures oM = new owaMeasures(situation);
	
						//computation of entropy
						oM.balance();
						System.out.println("Balance: "+oM.getBalance());
						
						docMessage = MakeFile.addMessageToLog(docMessage, "OK",false);
						
						XMCDADocument docMeasures = MakeFile.makeDocumentResultsMeasures(oM.calculated(), situation);
						MakeFile.createDocument(output+OUTMEASURESFILE, docMeasures);
						
					} else {
						docMessage = MakeFile.addMessageToLog(docMessage, "Fail information:\n"+situation.getErrorText(),true);
					}
				}
			}
		} catch (NullPointerException ex) {
			docMessage = MakeFile.addMessageToLog(docMessage, "The pathname argument is null",true);
		} catch (XmlException e) {
			for(Object error : e.getErrors()){
				XmlError xmlError = (XmlError)error;
				docMessage = MakeFile.addMessageToLog(docMessage, xmlError.getMessage(),true);
				docMessage = MakeFile.addMessageToLog(docMessage, xmlError.getCursorLocation().xmlText(),true);
			}
			docMessage = MakeFile.addMessageToLog(docMessage, "Error processing, parsing, or compiling XML",true);
		} catch (IOException e) {
			docMessage = MakeFile.addMessageToLog(docMessage, "File fail",true);
		} catch (ParseException e) {
			docMessage = MakeFile.addMessageToLog(docMessage, "Parse fail",true);
		} finally {
			MakeFile.createDocument(output+OUTMESSAGEFILE,docMessage);
		}
    }
}
