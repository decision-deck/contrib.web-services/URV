package componentes;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.util.LinkedHashSet;
import java.util.Iterator;
import java.util.Set;

public class Criterios {
	
	Set<Criterio> listS;
	
	public Criterios() {
		listS = new LinkedHashSet<Criterio>();
	}
	
	public boolean addCriterion(Criterio Cri){
		return listS.add(Cri);
	}
	
	public int size(){
		return listS.size();
	}
	
	public boolean isEmpty(){
		return listS.isEmpty();
	}
	
	public boolean removeCriterion(Criterio Cri){
		return listS.remove(Cri);
	}
	
	public void removeAllCriterion(){
		listS.clear();
	}
	
	public Criterio getCriterion(int i){
		Iterator<Criterio> it = listS.iterator();
		int j = 0;
		while (it.hasNext()) {
			Criterio aux = it.next();
		    if(i==j) return aux;
		    j++;
		}
		return null;
	}
	
	public Criterio[] getAllCriterion(){
		Criterio [] obj = new Criterio[listS.size()];
		Iterator<Criterio> it = listS.iterator();
		int i = 0;
		while (it.hasNext()) {
		    obj[i++]=it.next();
		}
		return obj;
	}
	
	public String toString(){
		String text = "";
		Iterator<Criterio> it = listS.iterator();
		while (it.hasNext()) {
		    Criterio element = it.next();
		    text += "Id: "+element.getId()+", Nom: "+element.getName()+".\n"; 
		}
		return text;
	}
	
}
