package Utils;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 * Opcio A
 * 1. orderar vector a agregar
 * 2. Generar vector pesos
 * 3. Generar vector auxiliar de pesos
 * 4. Recorrer auxiliar desde el final agregant de dos en dos
 * 
 * Opcio B
 * 1. Generar vector pesos
 * 2. orderar vector a agregar
 * 3. Generar vector auxiliar de pesos
 * 4. Recorrer auxiliar desde el final agregant de dos en dos
 * 
 */

import java.util.Arrays;
import java.util.HashMap;

import componentes.Alternativa;
import componentes.Criterio;
import componentes.Label;
import componentes.Situation;
import componentes.Weights;

public class ULOWA {
	
	Situation situation;
	private HashMap<Alternativa,Label> res;
	Label[] labels;
	Weights weights;
	
	public ULOWA(Situation situation) {
		
		res = new HashMap<Alternativa,Label>();
		this.labels = situation.getFuzzyVariable().getAllLabel();
		this.situation = situation;
		this.weights = situation.getWeights();
		
	}
	
	public HashMap<Alternativa,Label> calculated()
    {
    	Label[] labels2agg = new Label[situation.getCriteriaCount()];
    	for (Alternativa alter : situation.getAlternatives().getAllAlternative()){
    		int i = 0;
    		for(Criterio crit: situation.getCriteria().getAllCriterion()){
    			String nameLabel = situation.getEvaluation(alter, crit);
    			labels2agg[i++] = situation.getFuzzyVariable().getLabel(nameLabel);
    		}
    		Label resultat = aggregationULOWA(labels2agg, labels, weights.getWeightsValues());
    		res.put(alter, resultat);
		}
        return res;
    }
	
	
	public Label aggregationULOWA(Label[] labels2aggregate, Label[] labels, float[] w){
		
		Label result = labels[0];
		
		if(labels2aggregate.length>=2){
			
			//1. Ordenar les labels a agregar
			Arrays.sort(labels2aggregate);
			  
			System.out.print("  Sorted labels: ");
			for(int i=0;i<labels2aggregate.length;i++)
				System.out.print(labels2aggregate[i].getId()+" ");
			System.out.println("");
			
			//2. Generar el vector de pesos segons politica i nombre de labels
			/*Aquino el generem, sino entra per el fitxer*/
			
			//3. Calcular pesos auxiliars
			float[] w2 = calculateAuxWeigts(w);
			
			System.out.print("  Auxiliary weights: ");
			for(int i=0;i<w2.length;i++)
				System.out.print(w2[i]+" ");
			System.out.println("");
			
			//4. Aggregar recursivament des del final
			int a_pos=labels2aggregate.length-2;
			int b_pos=labels2aggregate.length-1;
			Label a=null;
			Label b=labels2aggregate[b_pos];
			while( a_pos > -1 ){
				System.out.println("Pos: "+a_pos);
				a = labels2aggregate[a_pos];
				System.out.println("AGGREGATE "+a.getId()+" "+b.getId()+" ");
				System.out.println("w2: "+w2[a_pos]+", Labels lenght:"+labels.length);
				b = aggregateLabelsPairwiseULOWA(w2[a_pos], a, b, labels);
				System.out.println(" --> "+b.getId());
				a_pos--;
			}
			result=b;
		}
		return result;
	}
	
	/**
	 * 
	 * @param weight Pes etiqueta a
	 * @param a Etiqueta que volem agregar
	 * @param b --> El pes �s (1-pes(a)), Etiqueta que volem agregar
	 * @param labels On treballem. Domini de etiquetes.
	 * @return Eqtiqueta resultant.
	 */
	  
	public Label aggregateLabelsPairwiseULOWA(float weight, Label a, Label b, Label[] labels){
		Label res=null;
		if(a.getIndex() < b.getIndex()){
			Label temp=a;
			a=b; b=temp; weight=1-weight;
		}
		
		if(Math.abs(a.getIndex()-b.getIndex())==1){
			if(b.getIndex()==0){
				System.out.println("RETURN "+a.getId());
				return a;
			}else if(a.getIndex()==labels.length-1){
				System.out.println("RETURN "+b.getId());
				return b;
			}
		}
		
		float intermediate = calculateIntermediate(a,b,weight);
		System.out.println("Intermediate -- "+intermediate+", Labels lenghts: "+labels.length);
		float max_sim=0;
		int c_pos=0;
		for(c_pos=b.getIndex();c_pos<=a.getIndex();c_pos++){
			System.out.println("------------------");
			System.out.println("c_pos: "+c_pos);
			System.out.println("------------------");
			float sim = calculateSimilarity(labels[c_pos],intermediate);
			if(sim>max_sim){
				max_sim=sim;
				res=labels[c_pos];
			}
		}
		//System.out.println("Aggregated "+a.getId()+" and "+b.getId()+" as: "+res.getId());
		return res;
	}
	
	public static float[] calculateAuxWeigts(float[] w){
		float[] aux=new float[w.length];
		float ant;
		aux[0]=w[0];
		ant=1-w[0];
		for(int i=1;i<aux.length;i++){
			if (ant==0) aux[i]=0;
			else {
			  aux[i]=w[i]/ant;
			  ant=ant-w[i];
			}
		}
		return aux;
	}

	/*Busca similitud entre el valor trobat i les altres etiquetes.*/
	private float calculateSimilarity(Label a, float intermediate){
		System.out.println(a.getId()+" : {"+a.getPoints()[0]+","+a.getPoints()[1]+","+a.getPoints()[2]+","+a.getPoints()[3]+"}");
		float prod=1;
		for(int i=0;i<4;i++){
			prod=prod*(2-Math.abs((a.getPoints())[i]-intermediate));
		}
		System.out.println(prod);
		float sim=(float) (Math.pow(prod,0.25)-1);
		System.out.println("SIM of "+a.getId()+" with "+intermediate+" is "+sim);
		return sim;
	}
	
	/*Important calcull intermig segons un pes*/
	private float calculateIntermediate(Label a, Label b, float weight){
		float[] coga=a.getCoG();
		float[] cogb=b.getCoG();
		float x=coga[0]+(1-weight)*(cogb[0]-coga[0]);
		System.out.println("Intermediate of "+coga[0]+" and "+cogb[0]+" with w="+weight+" is "+x);
		return x;
	}
	
}
