package componentes;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class Alternativas{
	
	private Set<Alternativa> listA;
	
	public Alternativas() {
		listA = new LinkedHashSet<Alternativa>();
	}
	
	public boolean addAlternative(Alternativa alt){
		return listA.add(alt);
	}
	
	public int size(){
		return listA.size();
	}
	
	public boolean isEmpty(){
		return listA.isEmpty();
	}
	
	public boolean removeAlternative(Alternativa alt){
		return listA.remove(alt);
	}
	
	public void removeAllAlternatives(){
		listA.clear();
	}
	
	public Alternativa getAlternative(int i){
		Iterator<Alternativa> it = listA.iterator();
		int j = 0;
		while (it.hasNext()) {
		    Alternativa aux = it.next();
		    if(i==j) return aux;
		    j++;
		}
		return null;
	}
	
	public Alternativa[] getAllAlternative(){
		
		Alternativa[] obj = new Alternativa[listA.size()]; 
		Iterator<Alternativa> it = listA.iterator();
		int i = 0;
		while (it.hasNext()) {
		    obj[i++]=it.next();
		}
		return obj;
	}
	
	public String toString(){
		String text = "";
		Iterator<Alternativa> it = listA.iterator();
		while (it.hasNext()) {
		    Alternativa element = it.next();
		    text += "Id: "+element.getId()+", Nom: "+element.getName()+".\n"; 
		}
		return text;
	}
}
