package componentes;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

public class Label implements Comparable<Label>{
	
	/*Requirement*/
	private String id;
	
	/*Opcional*/
	private String name;
	private int index;
	private float[] points;
	
	public Label(String id,float a,float b,float c,float d,int index){
		this.id = id;
		this.name = null;
		this.index = index;
		
		/*fuzzy Points - Always 4 (that's a Triangle o Trapezy)*/
		this.points=new float[4];
		this.points[0]=a;
		this.points[1]=b;
		this.points[2]=c;
		this.points[3]=d;
	}
	
	public Label(String id,String name,float a,float b,float c,float d,int index){
		this.name = name;
		this.id = id;
		this.index = index;
		
		/*fuzzy Points - Always 4 (that's a Triangle o Trapezy)*/
		this.points=new float[4];
		this.points[0]=a;
		this.points[1]=b;
		this.points[2]=c;
		this.points[3]=d;
	}
	
	public Label(String id ,String name ,int index ,float[] points ){
		this.name = name;
		this.id = id;
		this.index = index;
		
		/*fuzzy Points - Always 4 (that's a Triangle o Trapezy)*/
		this.points = points.clone();
	}
	
	public Label(String id ,int index ,float[] points ){
		this.name = null;
		this.id = id;
		this.index = index;
		
		/*fuzzy Points - Always 4 (that's a Triangle o Trapezy)*/
		this.points = points.clone();
	}
	
	public float[] getPoints() {
		return points;
	}

	public int getIndex() {
		return index;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

	/*Important Centre de gra --> Retorna tupla (x, y)*/
	public float[] getCoG(){
		float[] CoG = new float[2];
		float a1 = points[0], a2 = points[1], a3 = points[2], a4 = points[3];
		  
		if(a1!=a4){	CoG[1]=(1.0f/6.0f)*(((a3-a2)/(a4-a1))+2f); }
		else { CoG[1]=0.5f;	}
		
		CoG[0]=(CoG[1]*(a3+a2)+((a4+a1)*(1-CoG[1])))/2+0.001f;
		return CoG;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Label)) {
			return false;
		}
		Label other = (Label) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		final StringBuffer str = new StringBuffer("Alternative");
		str.append(" [");
		str.append(id);
		if (name != null) {
		    str.append(", '" + name + "'");
		}
		str.append(" Index: " + index);
		str.append(" (");
		str.append(points[0]+", ");
		str.append(points[1]+", ");
		str.append(points[2]+", ");
		str.append(points[3]);
		str.append(") ]");
		return str.toString();
	}

	@Override
	public int compareTo(Label o) {
		if( this.index < o.getIndex() ) return 1;
		else if( this.index > o.getIndex() ) return -1;
		return 0;
	}
	
}
