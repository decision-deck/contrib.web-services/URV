package componentes;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.util.ArrayList;

public class Weights {

	public static final double IMPRECISION = 1e-6f;
	private ArrayList<Weight> map ;

	public Weights(){
		map = new ArrayList<Weight>();
	}

	public int size() {
		return map.size();
	}

	public void addWeights(float[] weights){
		for(float weight : weights){
			this.map.add(new Weight(weight));
		}
	}

	public void addWeight(Weight weight){
		map.add(weight);
	}

	public Weight[] getWeights() {
		Weight[] weight = new Weight[map.size()];
		int i = 0;
		for( i=0;i<map.size();i++  ) {
			weight[i] = map.get(i);
		}
		return weight;
	}

	public float[] getWeightsValues() {
		float[] weight = new float[map.size()];
		int i = 0;
		for( i=0;i<map.size();i++  ) {
			weight[i] = (float) map.get(i).getValue();
		}
		return weight;
	}

	public double getSum() {
		double sum = recompute();
		return sum;
	}

	public boolean isNormalized()
	{
		double sum = recompute();
		final boolean sumToOne = Math.abs(sum - 1f) < IMPRECISION;
		return sumToOne;
	}

	private double recompute()
	{
		double sum = 0;
		for (final Weight weight : getWeights()) {
			final double value = weight.getValue();
			sum = sum + value;
		}
		return sum;
	}

	public void generateWeightVector(int n, String policy){
		float[] w=new float[n];
		float a=0.0f,b=1.0f;
		if (policy.equals("MOST")){
			a=0.0f;
			b=0.8f;
		}else if(policy.equals("AT_LEAST_HALF")){
			a=0.0f;
			b=0.5f;
		}else if(policy.equals("AS_MANY_AS_POSSIBLE")){
			a=0.5f;
			b=1.0f;
		}else if(policy.equals("AVERAGE")){
			a=0.0f;
			b=1.0f;
		}else if(policy.equals("WORST")){
			a=0.7f;
			b=0.9f;
		}
		for(int i=1;i<=n;i++){
			w[i-1]=Q(a,b,((float)i/(float)n))-Q(a,b,((float)(i-1)/(float)n));
		}
		System.out.print("  Weights: ");
		for(float val:w){
			System.out.print(val+" ");
		}
		System.out.println("");
		
		for(float weight : w){
			this.map.add(new Weight(weight));
		}
		
	}

	public static float Q(float a, float b, float r){
		//System.out.println(r);
		if(r<a) return 0f;
		else if (r>b) return 1f;
		else return ((r-a)/(b-a));
	}

}
