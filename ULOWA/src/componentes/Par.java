package componentes;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

public class Par<Type1, Type2> {

    private final Type1 element1;
    private final Type2 element2;
    
    public Par(final Type1 element1, final Type2 element2) {
    	this.element1 = element1;
    	this.element2 = element2;
    }

    @Override
    public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Par<?, ?> other = (Par<?, ?>) obj;
		if (element1 == null) {
			if (other.element1 != null) {
			return false;
			}
		} else if (!element1.equals(other.element1)) {
			return false;
		}
		if (element2 == null) {
			if (other.element2 != null) {
			return false;
			}
		} else if (!element2.equals(other.element2)) {
			return false;
		}
		return true;
    }

    public String getDebugStr() {
    	return "" + element1 + ", " + element2;
    }

    public Type1 getElement1() {
    	return element1;
    }

    public Type2 getElement2() {
    	return element2;
    }

    @Override
    public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((element1 == null) ? 0 : element1.hashCode());
		result = prime * result + ((element2 == null) ? 0 : element2.hashCode());
		return result;
    }

    @Override
    public String toString() {
    	return getClass().getSimpleName() + " [" + getDebugStr() + "]";
    }

}
