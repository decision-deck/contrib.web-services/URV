package main;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.decisionDeck.x2012.xmcda220.Alternative;
import org.decisionDeck.x2012.xmcda220.AlternativeOnCriteriaPerformances;
import org.decisionDeck.x2012.xmcda220.AlternativeOnCriteriaPerformances.Performance;
import org.decisionDeck.x2012.xmcda220.AlternativeValue;
import org.decisionDeck.x2012.xmcda220.Alternatives;
import org.decisionDeck.x2012.xmcda220.AlternativesValues;
import org.decisionDeck.x2012.xmcda220.CategoriesValues;
import org.decisionDeck.x2012.xmcda220.CategoryValue;
import org.decisionDeck.x2012.xmcda220.Criteria;
import org.decisionDeck.x2012.xmcda220.Criterion;
import org.decisionDeck.x2012.xmcda220.PerformanceTable;
import org.decisionDeck.x2012.xmcda220.FuzzyNumber.Trapezoidal;
import org.decisionDeck.x2012.xmcda220.Value;
import org.decisionDeck.x2012.xmcda220.Values;
import org.decisionDeck.x2012.xmcda220.XMCDADocument;
import org.decisionDeck.x2012.xmcda220.XMCDADocument.XMCDA;

import componentes.Alternativa;
import componentes.Alternativas;
import componentes.Criterio;
import componentes.Criterios;
import componentes.FuzzyVariable;
import componentes.Label;
import componentes.Matrix;
import componentes.Par;
import componentes.Situation;
import componentes.Weight;
import componentes.Weights;

public class ReadXMCDA {
	
	public static FuzzyVariable readFuzzy(XMCDA xmcda)
	{
		CategoriesValues cats = xmcda.getCategoriesValuesArray(0);
		CategoryValue cat = cats.getCategoryValueArray(0);
		Values values = cat.getValuesArray(0);
		Value[] value = values.getValueArray();
		FuzzyVariable fuzzySet = new FuzzyVariable();
		for (int i = 0; i < value.length; i++)
        {
			String id = value[i].getId();
			Trapezoidal trapezoidal = value[i].getFuzzyNumber().getTrapezoidal();
			Value val;
			Float a,b,c,d;
			/*Point a*/
			val = trapezoidal.getPoint1().getAbscissa();
			if ( val.isSetInteger() ){ a = (float) val.getInteger(); }
			else if ( val.isSetReal() ) { a = val.getReal(); }
			else { a = 0f; }
			/*Point b*/
			val = trapezoidal.getPoint2().getAbscissa();
			if ( val.isSetInteger() ){ b = (float) val.getInteger(); }
			else if ( val.isSetReal() ) { b = val.getReal(); }
			else { b = 0f; }
			/*Point c*/
			val = trapezoidal.getPoint3().getAbscissa();
			if ( val.isSetInteger() ){ c = (float) val.getInteger(); }
			else if ( val.isSetReal() ) { c = val.getReal(); }
			else { c = 0f; }
			/*Point d*/
			val = trapezoidal.getPoint4().getAbscissa();
			if ( val.isSetInteger() ){ d = (float) val.getInteger(); }
			else if ( val.isSetReal() ) { d = val.getReal(); }
			else { d = 0f; }
			if( value[i].isSetName() ){
				String name = value[i].getName();
				fuzzySet.setLabel(new Label(id, name, a, b, c, d, i));
			} else {
				fuzzySet.setLabel(new Label(id, a, b, c, d, i));
			}
			
        }
		return fuzzySet;
		
	}
	
	public static Alternativas readAlternatives(XMCDA xmcda)
	{
		Alternatives alts = xmcda.getAlternativesArray(0);
		Alternative[] alt = alts.getAlternativeArray();
		Alternativas alters = new Alternativas();
		for (int i = 0; i < alt.length; i++)
        {
			boolean[] active = alt[i].getActiveArray();
			Alternativa alter;
        	if(active.length>0){
        		alter = new Alternativa(alt[i].getId(), alt[i].getName(),active[0]);
        	} else {
        		alter = new Alternativa(alt[i].getId(), alt[i].getName(),true);
        	}
			alters.addAlternative(alter);
        }
		return alters;
	}

	public static Criterios readCriteria(XMCDA xmcda)
	{
		Criteria crit = xmcda.getCriteriaArray(0);
        Criterion[] criterion = crit.getCriterionArray();
        Criterios Cri = new Criterios();
        for( int i = 0; i<criterion.length; i++) {
        	boolean[] active = criterion[i].getActiveArray();
        	Criterio criton;
        	if(active.length>0){
        		criton = new Criterio(criterion[i].getId(), criterion[i].getName(),active[0]);
        	} else {
        		criton = new Criterio(criterion[i].getId(), criterion[i].getName(),true);
        	}
        	Cri.addCriterion(criton);
        }
		return Cri;
	}

	public static Weights readWeights(XMCDA xmcda)
	{
		AlternativesValues AltersValues = xmcda.getAlternativesValuesArray(0);
		AlternativeValue AlterValue = AltersValues.getAlternativeValueArray(0);
		Weights weights = new Weights();
		Values values = AlterValue.getValuesArray(0);
		Value[] value = values.getValueArray();
		for (int x = 0; x < value.length; x++)
        {
			float a = 0;
			if ( value[x].isSetInteger() ){ a = (float) value[x].getInteger(); }
			else if ( value[x].isSetReal() ) { a = value[x].getReal(); }
			else { a = 0f; }
			Weight weight = new Weight(a);
			weights.addWeight(weight);
        }
		return weights;
	}
	
	public static Matrix<Par<Alternativa,Criterio>,String> readPreference(XMCDA xmcda)
	{
		PerformanceTable performanceTable = xmcda.getPerformanceTableArray(0);
		AlternativeOnCriteriaPerformances[] alternativePerformance = performanceTable.getAlternativePerformancesArray();
		
		Matrix<Par<Alternativa, Criterio>, String> matrix = new Matrix<Par<Alternativa,Criterio>, String>();
		for (int i = 0; i < alternativePerformance.length; i++)
        {
			String Id = alternativePerformance[i].getAlternativeID();
			
			Performance[] performance = alternativePerformance[i].getPerformanceArray();
			Alternativa alter = new Alternativa(Id,true);
			
			for(int x = 0;x<performance.length;x++)
			{
				String Id2 = performance[x].getCriterionID();
				Criterio criton = new Criterio(Id2,true);
				Par<Alternativa, Criterio> pair = new Par<Alternativa, Criterio>(alter, criton);
				Value value = performance[x].getValue();
				if( value.isSetLabel() && value.getLabel() != ""){
					matrix.setValue(pair, value.getLabel());
				}else{
					matrix.setValue(pair, null);
				}
			}
        }
		return matrix;
	}

	public static boolean validateXml(XmlObject xml, ArrayList<XmlError> errorList)
	{
		XmlOptions validateOptions = new XmlOptions();
		validateOptions.setErrorListener(errorList);
		boolean isValid = xml.validate(validateOptions);
		return isValid;
	}
	
	public static Situation readData(File[] listFile, ArrayList<XmlError> errorList) throws XmlException, IOException{
    	Situation situation = new Situation();
    	for (int x=0;x<listFile.length;x++){
    		System.out.println(listFile[x].getName());
			File f = listFile[x];
			
			/*When a document is parse, if it have error throw the XmlException, for all caseses.
			 * It's important put the correct NameSpace or not validated the file.*/
			XMCDADocument xd = XMCDADocument.Factory.parse(f);
			if(validateXml(xd,errorList)){
				XMCDA xmcda = xd.getXMCDA();
		    	Alternatives[] xmlAltsList = xmcda.getAlternativesArray();
				if (xmlAltsList.length > 0) {
					Alternativas alts = readAlternatives(xmcda);
					situation.setAlternatives(alts);
				}
				Criteria[] xmlCritsList = xmcda.getCriteriaArray();
				if (xmlCritsList.length > 0) {
					Criterios crits = readCriteria(xmcda);
					situation.setCriteria(crits);
				}

				PerformanceTable[] xmlPerfTablesList = xmcda.getPerformanceTableArray();
				if (xmlPerfTablesList.length > 0) {
					Matrix<Par<Alternativa, Criterio>, String> matrix = readPreference(xmcda);
					situation.setEvaluationMatrix(matrix);
				}
				AlternativesValues[] xmlCritsValsList = xmcda.getAlternativesValuesArray();
				if (xmlCritsValsList.length > 0) {
					Weights pesos = readWeights(xmcda);
					situation.setWeights(pesos);
				}
				CategoriesValues[] xmlFuzzyList = xmcda.getCategoriesValuesArray();
				if (xmlFuzzyList.length > 0) {
					situation.setFuzzyVariable(readFuzzy(xmcda));
				}
			} else { 
				System.out.println("Error file "+listFile[x].getName());
				break;
			}
    	}
    	return situation;
	}
}
