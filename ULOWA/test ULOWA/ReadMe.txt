Example without errors
----------------------

in1, out1: With 2 criterion and 5 alternatives. Weight policy -> Average

in2, out2: With 5 criterion and 2 alternative. Weight policy -> Average

in3, out3: With 5 criterion and 2 alternative. Weight policy -> Most

in4, out4: With incorrect XMCDA syntaxis, in file "alternative.xml".

in5, out5: The id of one alternative is different from id of performanceTable. In file "alternatives.xml".

in6, out6: The weights aren't normalized. In file "weights.xml".

in7, out7: The number of weights and the number of criteria are different. In file "weights.xml".

in8, out8: Put in performanceTable a label that does not exist. In file "performanceTable.xml".

in9, out9: Repeat the id of the performance table. In file "performanceTable.xml".