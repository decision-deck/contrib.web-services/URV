Measures on Linguistic Variables
 
When fuzzy sets are associated to linguistic labels on a given variable, one can calculate some 
descriptors of those fuzzy values.
In this module we calculate the two classic measures called: specificity and fuzziness.

Authors:
			- Aida Valls. <aida.valls@urv.cat>
			- Oualid Mohamed Benkarim
			
 Universitat Rovira i Virgili (URV), Tarragona, Spain
 Escola Tècnica Superior d'Enginyeria, Departament d'Enginyeria Informàtica i Matemàtiques.
 
 version 1.0
 
 2013
 
 --> to run on windows: You must run the file TestSpecFuz.bat
 
 
 --> parameters required for running the ULOWA module are: 
 -i in -o out
 