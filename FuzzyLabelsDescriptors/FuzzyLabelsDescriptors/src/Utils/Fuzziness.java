package Utils;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import componentes.FuzzyVariable;
import componentes.Label;

/**
 * 
 * Class to compute the fuzziness of a fuzzy variable's fuzzy sets
 * 
 */

public class Fuzziness {
	
	Label[] labels;
	
	/**********************************************************************
	 * Constructor1 given the FuzzyVariable
	 **********************************************************************/	
	public Fuzziness(FuzzyVariable fuzzyVar) 
	{	
		this.labels = fuzzyVar.getAllLabel();
	}

	/**********************************************************************
	 * Constructor2 given the Labels
	 **********************************************************************/
	public Fuzziness(Label[] labels) 
	{	
		this.labels = labels;
	}
	

	/**********************************************************************
	 * Calculate fuzziness for each fuzzy set
	 **********************************************************************/
	public LinkedHashMap<Label, Double> calculated() throws Exception 
    {
		if (labels.length>0 && (labels[0].getPoints()[0]!=0 || 
				labels[labels.length-1].getPoints()[3]!=1))
			throw new Exception("Required range for fuzzy variable: [0...1]");
		
		LinkedHashMap<Label, Double> results = new LinkedHashMap<Label, Double>();
		
		for(int i=0; i<labels.length; ++i) {
			results.put(labels[i], computeFuzziness(labels[i]));
		}
				
		return results;
    }
	
	
	/**********************************************************************
	 * Calculates fuzziness for a given label (fuzzy set)
	 * Instead of using an integration method, the area under both triangles 
	 * is calculated  
	 **********************************************************************/
	private double computeFuzziness(Label l)
	{
		BigDecimal bigVal;
		
		double h = l.getHeight();
		float[] points = l.getPoints();
		
		double area = points[1]>points[0]?((points[1]-points[0]) * h/2):0;
		area += points[3]>points[2]?((points[3]-points[2]) * h/2):0;
		
		//Round up to 2 decimal places
		bigVal = BigDecimal.valueOf(area);
	    bigVal = bigVal.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		
		return bigVal.doubleValue();
	}
	
	
	@Override
	public String toString() 
	{
		String s = "*********************************************\nFuziness:\n";
		Iterator<Entry<Label, Double>> it = null;
		
		try{
			it = this.calculated().entrySet().iterator();	
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		while (it.hasNext()) 
		{
	        Entry<Label, Double> pairs = (Entry<Label, Double>)it.next();
	        s += "\t" + pairs.getKey().getId() + " = " + pairs.getValue() + "\n";
	    }
		s += "*********************************************\n\n";
		
		return s;
	}
}

