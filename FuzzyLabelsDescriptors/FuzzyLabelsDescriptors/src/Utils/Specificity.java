package Utils;


import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import componentes.FuzzyVariable;
import componentes.Label;


/**
 * 
 * Class to compute the specificity of a fuzzy variable's fuzzy sets
 * 
 */

public class Specificity {
	
	Label[] labels;
	
	/**********************************************************************
	 * Constructor1 given the FuzzyVariable
	 **********************************************************************/
	public Specificity(FuzzyVariable fuzzyVar) 
	{	
		this.labels = fuzzyVar.getAllLabel();
	}

	/**********************************************************************
	 * Constructor2 given the Labels
	 **********************************************************************/
	public Specificity(Label[] labels) 
	{	
		this.labels = labels;
	}

	
	/**********************************************************************
	 * Calculates specificity for each fuzzy set (in this.labels)
	 * Returns a Hashmap where Label is a fuzzy set and the Double value 
	 * indicates the corresponding specificity
	 **********************************************************************/
	public LinkedHashMap<Label, Double> calculated() throws Exception
    {
		if (labels.length>0 && (labels[0].getPoints()[0]!=0 || 
				labels[labels.length-1].getPoints()[3]!=1))
			throw new Exception("Required range for fuzzy variable: [0...1]");

		
		LinkedHashMap<Label, Double> results = new LinkedHashMap<Label, Double>();
		
		for(int i=0; i<labels.length; ++i)
			results.put(labels[i], computeSpecificity(labels[i]));
		
		return results;
    }
	
	
	/**********************************************************************
	 * Calculates specificity for a given label (fuzzy set)
	 * Instead of using an integration method, the area under the fuzzy set 
	 * is calculated as a sum of the area under the left triangle, the central 
	 * square and the right triangle
	 * Note: some of these pieces might not exist
	 **********************************************************************/
	private double computeSpecificity(Label l)
	{
		BigDecimal bigVal;
		
		double h = l.getHeight();
		float[] points = l.getPoints();
		
		double area = points[1]>points[0]?((points[1]-points[0]) * h/2):0;
		
		area += points[2]>points[1]?((points[2]-points[1]) * h):0;
		area += points[3]>points[2]?((points[3]-points[2]) * h/2):0;
		
		//double res = points[3]>points[0]?(area/(points[3]-points[0])):0;
		double res = 1-area;
		
		//Round up to 2 decimal places
		bigVal = BigDecimal.valueOf(res);
	    bigVal = bigVal.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		
		return bigVal.doubleValue();
	}

	@Override
	public String toString() 
	{
		String s = "*********************************************\nSpecificity:\n";
		Iterator<Entry<Label, Double>> it = null;
		
		try{
			it = this.calculated().entrySet().iterator();	
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		while (it.hasNext()) 
		{
	        Entry<Label, Double> pairs = (Entry<Label, Double>)it.next();
	        s += "\t" + pairs.getKey().getId() + " = " + pairs.getValue() + "\n";
	    }
		s += "*********************************************\n\n";
		
		return s;
	}
	
	
	
}
