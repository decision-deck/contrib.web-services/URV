package main;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.xmlbeans.XmlCursor;
import org.decisionDeck.x2012.xmcda220.*;
import org.decisionDeck.x2012.xmcda220.XMCDADocument.*;

import componentes.Alternativa;
import componentes.Label;
import componentes.Situation;

public class MakeFile {

    public static void createDocument(String file, XMCDADocument doc){
		File xmlfile = new File(file);
		try{
			if(!xmlfile.exists()){
				xmlfile.createNewFile();
				xmlfile.setWritable(true);
			}
			doc.save(xmlfile);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}



    // makeDocumentResults for specificity and fuzziness, same template
	public static XMCDADocument makeDocumentResults(LinkedHashMap<Label, Double> resultat, Situation situation) {
		XMCDADocument doc = XMCDADocument.Factory.newInstance();
		XMCDA xmcda = doc.addNewXMCDA(); 
		AlternativesValues alternativesvalues = xmcda.addNewAlternativesValues();
		
		Iterator<Entry<Label,Double>> it = resultat.entrySet().iterator();
	    while (it.hasNext()) {
	        Entry<Label, Double> pairs = (Entry<Label, Double>)it.next();
	        AlternativeValue alternativevalue = alternativesvalues.addNewAlternativeValue();
    		alternativevalue.setAlternativeID(((Label)pairs.getKey()).getId());
    		alternativevalue.addNewValue().setReal(pairs.getValue().floatValue());
	    }
		
		return doc;
	}
	
	public static XMCDADocument makeDocumentResults(HashMap<Alternativa, Label> resultat, Situation situation) {
		XMCDADocument doc = XMCDADocument.Factory.newInstance();
		XMCDA xmcda = doc.addNewXMCDA(); 
		AlternativesValues alternativesvalues = xmcda.addNewAlternativesValues();
		for(Alternativa alter : situation.getAlternatives().getAllAlternative()){
			if (alter.isActive()){
				AlternativeValue alternativevalue = alternativesvalues.addNewAlternativeValue();
	    		alternativevalue.setAlternativeID(alter.getId());
	    		if (resultat.get(alter).getName() != null){
	    			alternativevalue.addNewValue().setLabel(resultat.get(alter).getName());
	    		}else{
	    			alternativevalue.addNewValue().setLabel(resultat.get(alter).getId());
	    		}
			}
		}
		return doc;
	}
	
	public static XMCDADocument addMessageToLog(XMCDADocument doc, String msg, boolean isError){
		XMCDA xmcda = doc.getXMCDA();
		XmlCursor orderCursor = xmcda.newCursor();
		orderCursor.toChild("methodMessages");
		MethodMessages methodmessages = (MethodMessages) orderCursor.getObject();
		if (isError){
			methodmessages.addNewErrorMessage().setText(msg);
		} else {
			methodmessages.addNewLogMessage().setText(msg);
		}
		return doc;
	}
	
	public static XMCDADocument makeMessageDocument() {
		XMCDADocument doc = XMCDADocument.Factory.newInstance();
		XMCDA xmcda = doc.addNewXMCDA(); 
		xmcda.addNewMethodMessages();
		return doc;
	}
}
