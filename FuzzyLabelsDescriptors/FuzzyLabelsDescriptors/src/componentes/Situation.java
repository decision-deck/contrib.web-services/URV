package componentes;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

public class Situation {
	
	FuzzyVariable fuzzyVariable;
	Weights weights;
	private Alternativas alts;
	private Criterios crits;
	private Matrix<Par<Alternativa, Criterio>, String> matrixEval;
	
	public Situation() {
		alts = new Alternativas();
		crits = new Criterios();
		matrixEval = new Matrix<Par<Alternativa, Criterio>, String>();
		fuzzyVariable = new FuzzyVariable();
		weights = new Weights();
	}
	
	/*ADDS*/
	/*-------------------------------------------------*/
	public void addFuzzyNumbers(Label label){
		fuzzyVariable.setLabel(label);
	}
	
	public boolean addAlternative(Alternativa alt) {
		return alts.addAlternative(alt);
	}

	public boolean addCriterion(Criterio criterion) {
		return crits.addCriterion(criterion);
	}
	
	public void putEvaluation(Alternativa row, Criterio column, String value) {
		matrixEval.setValue(new Par<Alternativa, Criterio>(row,column), value);
	}
	
	/*REMOVE*/
	/*-------------------------------------------------*/
	public boolean removeAlternative(Alternativa alt) {
		return alts.removeAlternative(alt);
	}

	public boolean removeCriterion(Criterio crit) {
		return crits.removeCriterion(crit);
	}
	
	/*GETS*/
	/*-------------------------------------------------*/
	public FuzzyVariable getFuzzyVariable() {
		return fuzzyVariable;
	}
	
	public Alternativas getAlternatives() {
		return alts;
	}

	public Criterios getCriteria() {
		return crits;
	}
	
	public Weights getWeights() {
		return weights;
	}
	
	public int getAlternativesCount() {
		return alts.size();
	}

	public int getCriteriaCount() {
		return crits.size();
	}
	
	public String getEvaluation(Alternativa row, Criterio column) {
		return matrixEval.getValue(new Par<Alternativa, Criterio>(row, column));
	}
	
	public Matrix<Par<Alternativa, Criterio>, String> getEvaluationMatrix() {
		return matrixEval;
	}
	
	public int getEvaluationsCount() {
		return matrixEval.size();
	}

	/*SETS*/
	/*-------------------------------------------------*/
	public void setFuzzyVariable(FuzzyVariable fuzzyVariable) {
		this.fuzzyVariable = fuzzyVariable;
	}
	
	public void setCriteria(Criterios crits) {
		if (crits == null) {
		    throw new NullPointerException("" + crits);
		}
		this.crits = crits;
	}
	
	public void setAlternatives(Alternativas alts) {
		if (alts == null) {
		    throw new NullPointerException("" + alts);
		}
		this.alts = alts;
	}
	
	public void setEvaluationMatrix(Matrix<Par<Alternativa,Criterio>,String> matrixEval) {
		if (matrixEval == null) {
		    throw new NullPointerException("" + matrixEval);
		}
		this.matrixEval = matrixEval;
	}
	
	public void setWeights(Weights weights) {
		this.weights = weights;
		
	}

	/*VALIDATINGS*/
	/*-------------------------------------------------*/
	public boolean existThisAlternative(String alternative){
		for (Label label : fuzzyVariable.getAllLabel()){
			if(label.getId().compareToIgnoreCase(alternative) == 0){
				return true;				
			}
		}
		return false;
	}
	
	public boolean validationSituation(){
		
		/*if(!weights.isNormalized()){
			return false;
		}
		if (crits.size()*alts.size()!=matrixEval.size()){
			return false;
		}
		if (crits.size()!=weights.size()){
			return false;
		}
		for (Alternativa alter : alts.getAllAlternative()){
			for (Criterio crit : crits.getAllCriterion()){
				if(matrixEval.getValue(new Par<Alternativa, Criterio>(alter, crit)) == null){
					return false;
				} else if (!existThisAlternative(matrixEval.getValue(new Par<Alternativa, Criterio>(alter, crit)))) {
					return false;
				}
			}
		}*/
		return true;
	}
	
	public String getErrorText(){
		String aux = "";
		for (Alternativa alter : alts.getAllAlternative()){
			for (Criterio crit : crits.getAllCriterion()){
				if(matrixEval.getValue(new Par<Alternativa, Criterio>(alter, crit)) == null){
					aux += "Alternative: "+alter.getId()+", Criterion: "+crit.getId()+": It's null\n";
				} else if (!existThisAlternative(matrixEval.getValue(new Par<Alternativa, Criterio>(alter, crit)))) {
					aux += "Alternative: "+alter.getId()+", Criterion: "+crit.getId()+": No exist this label\n";
				}
			}
		}
		if(!weights.isNormalized()){
			aux += "The weights aren't Normalized\n";
		}
		if (crits.size()*alts.size()!=matrixEval.size()){
			aux += "Check the number of alternatives and criteria based on the performanceTable\n";
		}
		if (crits.size()!=weights.size()){
			aux += "The number of weights and the number of criteria are different\n";
		}
		
		return aux;
	}
	
	/*METODS*/
	/*-------------------------------------------------*/
	public void generateWeights(Weight weight){
		this.weights.addWeight(weight);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Situation [");
		builder.append(fuzzyVariable.size() + " " + (fuzzyVariable.size() == 1 ? "fuzzy" : "fuzzys") + ", ");
		builder.append(alts.size() + " " + (alts.size() == 1 ? "alt" : "alts") + ", ");
		builder.append(crits.size() + " " + (crits.size() == 1 ? "crit" : "crits") + ", ");
		builder.append(matrixEval.size() + " " + (matrixEval.size() == 1 ? "eval" : "evals") + ", ");
		builder.append(weights.size() + " " + (weights.size() == 1 ? "weight" : "weights"));
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alts == null) ? 0 : alts.hashCode());
		result = prime * result + ((crits == null) ? 0 : crits.hashCode());
		result = prime * result + ((fuzzyVariable == null) ? 0 : fuzzyVariable.hashCode());
		result = prime * result + ((matrixEval == null) ? 0 : matrixEval.hashCode());
		result = prime * result + ((weights == null) ? 0 : weights.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Situation)) {
			return false;
		}
		Situation other = (Situation) obj;
		if (alts == null) {
			if (other.alts != null) {
				return false;
			}
		} else if (!alts.equals(other.alts)) {
			return false;
		}
		if (crits == null) {
			if (other.crits != null) {
				return false;
			}
		} else if (!crits.equals(other.crits)) {
			return false;
		}
		if (fuzzyVariable == null) {
			if (other.fuzzyVariable != null) {
				return false;
			}
		} else if (!fuzzyVariable.equals(other.fuzzyVariable)) {
			return false;
		}
		if (matrixEval == null) {
			if (other.matrixEval != null) {
				return false;
			}
		} else if (!matrixEval.equals(other.matrixEval)) {
			return false;
		}
		if (weights == null) {
			if (other.weights != null) {
				return false;
			}
		} else if (!weights.equals(other.weights)) {
			return false;
		}
		return true;
	}
	
}
