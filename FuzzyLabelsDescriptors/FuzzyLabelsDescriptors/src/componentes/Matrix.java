package componentes;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;

public class Matrix<Pair,Type1> { 

	Map<Pair,Type1> list = new HashMap<Pair,Type1>(); 

	public Matrix(){
		list = new HashMap<Pair,Type1>();  
	}

	public int size(){  
		return list.size();
	}

	public Type1 getValue(Pair key){
		return list.get(key);
	}  

	public void setValue(Pair key, Type1 value){  
		list.put(key, value);
	}
	
	public String toString(){
		String text = "";
		Collection<Type1> col = list.values();
		Iterator<Type1> it = col.iterator();
		while (it.hasNext()) {
		    Type1 element = it.next();
		    text += "Value: "+element.toString()+".\n"; 
		}
		return text;
    }

}