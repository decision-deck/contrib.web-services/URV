package componentes;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

public class Weight implements Comparable<Weight> {
	
    private final double weight;
    
    public Weight(final double weight) {
		if (weight < 0f) {
		    throw new IllegalArgumentException("Invalid weight: " + weight + ".");
		}
		this.weight = weight;
    }

    @Override
    public boolean equals(final Object obj) {
		if (this == obj) {
		    return true;
		}
		if (obj == null) {
		    return false;
		}
		if (getClass() != obj.getClass()) {
		    return false;
		}
		final Weight other = (Weight) obj;
		if (Double.doubleToLongBits(weight) != Double.doubleToLongBits(other.weight)) {
		    return false;
		}
		return true;
    }

    public double getValue() {
    	return weight;
    }

    @Override
    public int hashCode() {
		final int prime = 31;
		int result = 1;
		final long temp = Double.doubleToLongBits(weight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
    }

    @Override
    public String toString() {
    	return "Weight [" + weight + "]";
    }

    @Override
    public int compareTo(Weight w2) {
    	return Double.compare(weight, w2.weight);
    }
}
