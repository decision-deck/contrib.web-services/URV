#! /bin/sh

# make sure jars are readable by all
find build -type f -exec chmod a+r {} \;

f=OWA
pax -wjf build/$f.tar.bz2 \
      -s _build_${f}_ \
      $f/tests build/$f.jar

f=ULOWA
pax -wjf build/$f.tar.bz2 \
      -s _build_${f}_ \
      $f/in $f/out build/$f.jar

f=FuzzyLabelsDescriptors
pax -wjf build/$f.tar.bz2 \
      -s _${f}/__ -s _build_${f}_ \
      $f/$f/in $f/$f/out build/$f.jar

for f in divizLingCOG divizLingCOM divizLingOrdinal;
do
  pax -wjf build/$f.tar.bz2 \
      -s _LingToNum/__ -s _build_${f}_ \
      LingToNum/$f/in LingToNum/$f/out build/$f.jar
done

for f in OWA-WeightsBalance OWA-WeightsDivergence OWA-WeightsEntropy OWA-WeightsOrness;
do
  pax -wjf build/$f.tar.bz2 \
      -s _OWA-WeightsDescriptors/__ -s _build_${f}_ \
      OWA-WeightsDescriptors/$f/in OWA-WeightsDescriptors/$f/out build/$f.jar
done
