From a linguistic scale to numerical scale by COM method: Center of maximum.
 
Java implementation of a defuzzification of set of fuzzy labels according to their position.
The method consists in returning the point in the center of the interval where the membership function has
its maximum value. 
Given a set of alternatives that are associated to a linguistic fuzzy term (alternativeValues file)
a translation to the corresponding numerical value is made.

Authors:
			- Aida Valls. <aida.valls@urv.cat>
			- Hernan Fernandez Lescano
			
 Universitat Rovira i Virgili (URV), Tarragona, Spain
 Escola Tècnica Superior d'Enginyeria, Departament d'Enginyeria Informàtica i Matemàtiques.
 
 version 1.0
 
 2013
 
 --> to run on windows: You must run the file LingCOM.bat
 
  --> parameters required for running the module are: 
 -i in -o out
 


