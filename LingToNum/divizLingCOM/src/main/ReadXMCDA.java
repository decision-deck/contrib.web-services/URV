package main;


/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.decisionDeck.x2012.xmcda220.AlternativesValues;
import org.decisionDeck.x2012.xmcda220.CategoriesValues;
import org.decisionDeck.x2012.xmcda220.CategoryValue;
import org.decisionDeck.x2012.xmcda220.FuzzyNumber.Trapezoidal;
import org.decisionDeck.x2012.xmcda220.Value;
import org.decisionDeck.x2012.xmcda220.Values;
import org.decisionDeck.x2012.xmcda220.XMCDADocument;
import org.decisionDeck.x2012.xmcda220.XMCDADocument.XMCDA;

import componentes.FuzzyVariable;
import componentes.Label;
import componentes.Situation;


public class ReadXMCDA {
	
	
	public static FuzzyVariable readFuzzy(XMCDA xmcda)
	{
			
		CategoriesValues cats = xmcda.getCategoriesValuesArray(0);
		CategoryValue cat = cats.getCategoryValueArray(0);
		Values values = cat.getValuesArray(0);
		Value[] value = values.getValueArray();
		FuzzyVariable fuzzySet = new FuzzyVariable();
		for (int i = 0; i < value.length; i++)
        {
			String id = value[i].getId();
			Trapezoidal trapezoidal = value[i].getFuzzyNumber().getTrapezoidal();
			Value val;
			Float a,b,c,d;
			/*Point a*/
			val = trapezoidal.getPoint1().getAbscissa();
			if ( val.isSetInteger() ){ a = (float) val.getInteger(); }
			else if ( val.isSetReal() ) { a = val.getReal(); }
			else { a = 0f; }
			/*Point b*/
			val = trapezoidal.getPoint2().getAbscissa();
			if ( val.isSetInteger() ){ b = (float) val.getInteger(); }
			else if ( val.isSetReal() ) { b = val.getReal(); }
			else { b = 0f; }
			/*Point c*/
			val = trapezoidal.getPoint3().getAbscissa();
			if ( val.isSetInteger() ){ c = (float) val.getInteger(); }
			else if ( val.isSetReal() ) { c = val.getReal(); }
			else { c = 0f; }
			/*Point d*/
			val = trapezoidal.getPoint4().getAbscissa();
			if ( val.isSetInteger() ){ d = (float) val.getInteger(); }
			else if ( val.isSetReal() ) { d = val.getReal(); }
			else { d = 0f; }
			
			/* Name must be set to continue */
			if( value[i].isSetName() ){
				String name = value[i].getName();
				fuzzySet.setLabel(new Label(id, name, a, b, c, d, i));
			} 
			
        }
		return fuzzySet;
		
	}
	
	public static AlternativesValues readAlternativesValues(XMCDA xmcda){
		

		return xmcda.getAlternativesValuesArray(0);
		
	}
	
	
	
	public static boolean validateXml(XmlObject xml, ArrayList<XmlError> errorList)
	{
		XmlOptions validateOptions = new XmlOptions();
		validateOptions.setErrorListener(errorList);
		boolean isValid = xml.validate(validateOptions);
		return isValid;
	}
	
	public static Situation readData(File[] listFile, ArrayList<XmlError> errorList) throws XmlException, IOException{
    	Situation situation = new Situation();
    	for (int x=0;x<listFile.length;x++){
			File f = listFile[x];
			
			/*When a document is parse, if it have error throw the XmlException, for all cases.
			 * It's important put the correct NameSpace or not validated the file.*/
			
			XMCDADocument xd = XMCDADocument.Factory.parse(f);
			if(validateXml(xd,errorList)){
				
				XMCDA xmcda = xd.getXMCDA();
				AlternativesValues[] xmlAlternativesValsList = xmcda.getAlternativesValuesArray();
				if (xmlAlternativesValsList.length > 0) {
					 situation.setFuzzyValues(readAlternativesValues(xmcda));
				}
				CategoriesValues[] xmlFuzzyList = xmcda.getCategoriesValuesArray();
				if (xmlFuzzyList.length > 0) {
					situation.setFuzzyVariable(readFuzzy(xmcda));
				}
			} else { 
				System.out.println("Error file "+listFile[x].getName());
				break;
			}
    	}
    	
       	return situation;
	
	}
	
}
