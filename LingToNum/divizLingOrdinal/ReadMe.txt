From a linguistic scale to numerical scale
 
Java implementation of a defuzzification of set of fuzzy labels according to their position.
The first label is assigned the position 0.0, the second the position 1.0, and so on.
No interpretation based on the fuzzy membership function is done in this method.
Given a set of alternatives that are associated to a linguistic fuzzy term (alternativeValues file)
a translation to the corresponding numerical value is made.

Authors:
			- Aida Valls. <aida.valls@urv.cat>
			- Hernan Fernandez Lescano
			
 Universitat Rovira i Virgili (URV), Tarragona, Spain
 Escola Tècnica Superior d'Enginyeria, Departament d'Enginyeria Informàtica i Matemàtiques.
 
 version 1.0
 
 2013
 
 --> to run on windows: You must run the file LingOrdinal.bat
 
  --> parameters required for running the module are: 
 -i in -o out
 


