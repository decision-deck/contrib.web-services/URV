package componentes;

import java.util.HashMap;

import org.decisionDeck.x2012.xmcda220.AlternativesValues;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

public class Situation {
	
	FuzzyVariable fuzzyVariables;
	
	//this HashMap stores the relation between the alternative id and its linguistic value
	HashMap <String,String> altValues;
	
	AlternativesValues alternativas;
	
	String error = "";
	
	public Situation() {
		fuzzyVariables = new FuzzyVariable();
		altValues = new HashMap <String,String>();
	}
	
	
	
	
	
	/*GETS*/
	/*-------------------------------------------------*/
	public FuzzyVariable getFuzzyVariable() {
		return fuzzyVariables;
	}
	
	public HashMap<String,String> getAlternativesValues(){
		return altValues;
	}
	
	public String getAlternativaId(int i){
		
		return alternativas.getAlternativeValueArray(i).getAlternativeID();
	}
	
	public int getAlternativesSize(){
		return alternativas.sizeOfAlternativeValueArray();
	}
	/*SETS*/
	/*-------------------------------------------------*/
	public void setFuzzyVariable(FuzzyVariable fuzzyVariable) {
		this.fuzzyVariables = fuzzyVariable;
		//System.out.println(fuzzyVariables);
	}
	
	public void setFuzzyValues(AlternativesValues values){
		alternativas = values;
		
		
		//Filling the HashMap
		int i;
		for(i=0;i<values.sizeOfAlternativeValueArray();i++){
			altValues.put(values.getAlternativeValueArray(i).getAlternativeID(), values.getAlternativeValueArray(i).getValueArray(0).getLabel());
		}
		
	}

	/*VALIDATINGS*/
	/*-------------------------------------------------*/
	public boolean existThisAlternative(String alternative){
		for (Label label : fuzzyVariables.getAllLabel()){
			if(label.getId().compareToIgnoreCase(alternative) == 0){
				return true;				
			}
		}
		return false;
	}
	
	
	// A situation is valid if the labels of alternatives values match with
	// the labels of the fuzzy variables.
	public boolean validationSituation(){
		int i;
		for(i=0;i<alternativas.sizeOfAlternativeValueArray();i++){
		
			if(fuzzyVariables.getLabelByName(alternativas.getAlternativeValueArray(i).getValueArray(0).getLabel()) == null){
				
				error = "No coincidence between inputs labels and fuzzy variables labels";
				return false;
			}
		}
		
		return true;
	}
	
	public String getErrorText(){
		return error;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Situation [");
		builder.append(fuzzyVariables.size() + " " + (fuzzyVariables.size() == 1 ? "fuzzy tag" : "fuzzy tags") + ", ");
		builder.append(altValues.size() + " " + (altValues.size() == 1 ? "fuzzy alternative" : "fuzzy alternatives"));
		builder.append("]");
		return builder.toString();
	}
	
}