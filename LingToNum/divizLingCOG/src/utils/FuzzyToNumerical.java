package utils;

import java.util.HashMap;


import componentes.Situation;

public class FuzzyToNumerical {
	
	HashMap<String,Float> numerical;
	Situation situation;
	
	public enum Type{COG,ORDINAL,COM};
	
	Type type;
	
	public FuzzyToNumerical(Situation situation,Type type){
		this.situation = situation;
		this.type = type;
		numerical = new HashMap<String,Float>(); 
		
	}
	
	public Float defuzzy_by_COG(String s){
		
		Float f = null;
		String tag = situation.getAlternativesValues().get(s);
		f = situation.getFuzzyVariable().getLabelByName(tag).getCoG()[0];
		
		return f;
	}
	
	public Float defuzzy_by_COM(String s){
		
		Float f = null;
		String tag = situation.getAlternativesValues().get(s);
		f = situation.getFuzzyVariable().getLabelByName(tag).getCOM();
		
		return f;
	}
	
	public Float defuzzy_by_Ordinal(String s){
		
		Float f = null;
		String tag = situation.getAlternativesValues().get(s);
		f = (float)(situation.getFuzzyVariable().getLabelByName(tag).getIndex());

		
		return f;
	}
	public HashMap<String,Float> calculated(){
		
		int i;
		switch(type){
			case COG:{
				
				for (i=0;i<situation.getAlternativesSize();i++){
					numerical.put(situation.getAlternativaId(i), defuzzy_by_COG(situation.getAlternativaId(i)));
				}
				
				break;
			}
			
			case COM:{
				
				for (i=0;i<situation.getAlternativesSize();i++){
					numerical.put(situation.getAlternativaId(i), defuzzy_by_COG(situation.getAlternativaId(i)));
				}
				
				break;
			}
			
			case ORDINAL:{
				
				for (i=0;i<situation.getAlternativesSize();i++){
					numerical.put(situation.getAlternativaId(i), defuzzy_by_Ordinal(situation.getAlternativaId(i)));
				}
				
				break;
			}
			default:
			
		}
		
		
		return numerical;
		
	}
}
