package componentes;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;


public class FuzzyVariable{
	
	private Set<Label> listL;
	
	public FuzzyVariable() {
		listL = new LinkedHashSet<Label>();
	}
	
	public int size(){
		return listL.size();
	}
	
	public boolean isEmpty(){
		return listL.isEmpty();
	}
	
	public boolean labelExist(Label label){
		return listL.contains(label);
	}
	
	public boolean removeAlternative(Label alt){
		return listL.remove(alt);
	}
	
	public void removeAllAlternatives(){
		listL.clear();
	}
	
	public void setLabel(Label label){
		listL.add(label);
	}
	
	public Label getLabel(int i){
		Iterator<Label> it = listL.iterator();
		int j = 0;
		while (it.hasNext()) {
		    Label aux = it.next();
		    if(i==j) return aux;
		    j++;
		}
		return null;
	}
	
	public Label getLabel(String nameLabel){
		
		Iterator<Label> it = listL.iterator();
		while (it.hasNext()) {
		    Label aux = it.next();
		    if( aux.getId().compareToIgnoreCase(nameLabel) == 0 ) return aux;
		}
		return null;
	}
	
	public Label getLabelByName(String nameLabel){
		
		Iterator<Label> it = listL.iterator();
		while (it.hasNext()) {
		    Label aux = it.next();
		    if( aux.getName().compareToIgnoreCase(nameLabel) == 0 ) return aux;
		}
		return null;
	}
	
	public Label[] getAllLabel(){
		Label[] obj = new Label[listL.size()];
		Iterator<Label> it = listL.iterator();
		int i = 0;
		while (it.hasNext()) {
		    obj[i++]=it.next();
		}
		return obj;
	}
	
	public String toString(){
		String text = "";
		Iterator<Label> it = listL.iterator();
		while (it.hasNext()) {
			Label element = it.next();
		    text += "Id: "+element.getId()+", Nom: "+element.getName()+".\n"; 
		}
		return text;
	}

}
