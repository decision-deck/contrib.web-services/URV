From a linguistic scale to numerical scale
 
Java implementation of a defuzzification of set of fuzzy labels according to the COG method: Center of Gravity.
This method consists in calculating the center of gravity of each trapezoidal fuzzy set.
The trapezoidal set is defined in the FuzzyNumbers file.
Given a set of alternatives that are associated to a linguistic fuzzy term (alternativeValues file)
a translation to the corresponding numerical COG is made.

Authors:
			- Aida Valls. <aida.valls@urv.cat>
			- Hernan Fernandez Lescano
			
 Universitat Rovira i Virgili (URV), Tarragona, Spain
 Escola Tècnica Superior d'Enginyeria, Departament d'Enginyeria Informàtica i Matemàtiques.
 
 version 1.0
 
 2013
 
 --> to run on windows: You must run the file LingCOG.bat
 
  --> parameters required for running the module are: 
 -i in -o out
 


