package owa;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.util.HashMap;
import componentes.Alternativa;
import componentes.Criterio;
import componentes.Situation;

public class owa {
	
	private Situation situation;
	private HashMap<Alternativa,Double> res;
	
	public owa(Situation situation)
	{	
		if(situation == null)
		{
			throw new IllegalArgumentException("situation is null");
		}
		if(situation.getAlternativesCount()*situation.getCriteriaCount() != situation.getEvaluationsCount())
		{
			throw new IllegalArgumentException("Fail to data "+situation.toString());
		}
		if(situation.getCriteriaCount() != situation.getWeights().size())
		{
			throw new IllegalArgumentException("Fail to data "+situation.toString());
		}
		res = new HashMap<Alternativa,Double>();
		this.situation = situation;
	}
	
	private double[] descending(double[] dades)
    {
        double aux;
        int i,j;
        int numValues = dades.length;
        for (i=0;i<numValues;i++) {
            for (j=i+1;j<numValues;j++) {
                if ( dades[j] > dades[i] ) {
                    aux = dades[j];
                    dades[j] = dades[i];
                    dades[i] = aux;
                }
            }
        }
        return dades.clone();
    }
    
	private double[] preparaDades(Alternativa alter)
	{
		int i = 0;
		double[] dades = new double[situation.getCriteria().size()];
		for (Criterio criton : situation.getCriteria().getAllCriterion())
		{
			dades[i++] = situation.getEvaluation(alter, criton);
		}
		return dades;
	}
	
    private double escProd (double[] w,double[] dades)
    {
        double r;
        int i;
        int num_values = w.length;

        for (i=0,r=0.0; i<num_values; i++){
            r = r + ( w[i] * dades[i] );
        }
        return(r);
    }
    
    public HashMap<Alternativa,Double> calculated()
    {
    	double[] dades = new double[situation.getCriteriaCount()];
    	for (Alternativa alter : situation.getAlternatives().getAllAlternative()){
    		dades = this.descending(preparaDades(alter));
    		res.put(alter, escProd(situation.getWeights().getWeightsValues(),dades));
		}
        return res;
    }
}
