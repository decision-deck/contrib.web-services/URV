package componentes;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

public class Situation {
    
	private Alternativas alts;
	private Criterios crits;
	private Pesos weights;
	private Matrix<Par<Alternativa, Criterio>, Double> matrixEval;

	public Situation(){
		alts = new Alternativas();
		crits = new Criterios();
		weights = new Pesos();
		matrixEval = new Matrix<Par<Alternativa, Criterio>, Double>();
	}
    
	public boolean addAlternative(Alternativa alt) {
		return alts.addAlternative(alt);
	}

	public boolean addCriterion(Criterio criterion) {
		return crits.addCriterion(criterion);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Situation other = (Situation) obj;
		if (!alts.equals(other.alts)) {
			return false;
		}
		if (!crits.equals(other.crits)) {
			return false;
		}
		if (!matrixEval.equals(other.matrixEval)) {
			return false;
		}
		if (!weights.equals(other.weights)) {
			return false;
		}

		return true;
	}

	public Alternativas getAlternatives() {
		return alts;
	}

	public Criterios getCriteria() {
		return crits;
	}
	
	public int getAlternativesCount() {
		return alts.size();
	}

	public int getCriteriaCount() {
		return crits.size();
	}
	
	public Double getEvaluation(Alternativa row, Criterio column) {
		return matrixEval.getValue(new Par<Alternativa, Criterio>(row, column));
	}
	
	public Matrix<Par<Alternativa, Criterio>, Double> getEvaluationMatrix() {
		return matrixEval;
	}
	
	public int getEvaluationsCount() {
		return matrixEval.size();
	}
	
	public Pesos getWeights() {
		return weights;
	}

	@Override
	public int hashCode() {
		int prime = 31;
		int result = 1;
		result = prime * result + alts.hashCode();
		result = prime * result + crits.hashCode();
		result = prime * result + matrixEval.hashCode();
		result = prime * result + weights.hashCode();
		return result;
	}

	public boolean isEvaluationComplete() {
		return matrixEval.size() == ( alts.size() * crits.size() );
	}

	public void putEvaluation(Alternativa row, Criterio column, double value) {
		matrixEval.setValue(new Par<Alternativa, Criterio>(row,column), value);
	}

	public boolean removeAlternative(Alternativa alt) {
		return alts.removeAlternative(alt);
	}

	public boolean removeCriterion(Criterio crit) {
		return crits.removeCriterion(crit);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Situation [");
		builder.append(alts.size() + " " + (alts.size() == 1 ? "alt" : "alts") + ", ");
		builder.append(crits.size() + " " + (crits.size() == 1 ? "crit" : "crits") + ", ");
		builder.append(matrixEval.size() + " " + (matrixEval.size() == 1 ? "eval" : "evals") + ", ");
		builder.append(weights.size() + " " + (weights.size() == 1 ? "weight" : "weights"));
		builder.append("]");
		return builder.toString();
	}
	
	public void setCriteria(Criterios crits) {
		if (crits == null) {
		    throw new NullPointerException("" + crits);
		}
		this.crits = crits;
	}
	
	public void setAlternatives(Alternativas alts) {
		if (alts == null) {
		    throw new NullPointerException("" + alts);
		}
		this.alts = alts;
	}

	public void setWeights(Pesos weights) {
		if (weights == null) {
		    throw new NullPointerException("" + weights);
		}
		this.weights = weights;
	}
	
	public Pesos getCoalitions() {
		return weights;
	}
	
	public void setEvaluationMatrix(Matrix<Par<Alternativa,Criterio>,Double> matrixEval) {
		if (matrixEval == null) {
		    throw new NullPointerException("" + matrixEval);
		}
		this.matrixEval = matrixEval;
	}
	
	public boolean validationSituation(){
		
		if(!weights.isNormalized()){
			return false;
		}
		if (crits.size()*alts.size()!=matrixEval.size()){
			return false;
		}
		if (crits.size()!=weights.size()){
			return false;
		}
		for (Alternativa alter : alts.getAllAlternative()){
			for (Criterio crit : crits.getAllCriterion()){
				if(matrixEval.getValue(new Par<Alternativa, Criterio>(alter, crit)) == null){
					return false;
				}
			}
		}
		return true;
	}
	
	public String getErrorText(){
		String aux = "";
		for (Alternativa alter : alts.getAllAlternative()){
			for (Criterio crit : crits.getAllCriterion()){
				if(matrixEval.getValue(new Par<Alternativa, Criterio>(alter, crit)) == null){
					aux += "Alternative: "+alter.getId()+", Criterion: "+crit.getId()+"\n";
				}
			}
		}
		if(!weights.isNormalized()){
			aux += "The weights aren't Normalized\n";
		}
		if (crits.size()*alts.size()!=matrixEval.size()){
			aux += "Check the number of alternatives and criteria based on the performanceTable\n";
		}
		if (crits.size()!=weights.size()){
			aux += "The number of weights and the number of criteria are different\n";
		}
		
		return aux;
	}
    
}
