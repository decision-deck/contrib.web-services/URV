package componentes;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

public class RequiredID {

	private String id;
	private String name;
	private boolean active;
	
	public RequiredID(String id, String name, boolean active) {
		if (id != null && id.trim().length() == 0) {
		    throw new NullPointerException("" + id + name);
		}
		if (name != null && name.trim().length() == 0) {
		    throw new IllegalArgumentException(id);
		}
		this.id = id;
		this.name = name;
		this.active = active;
	}

	public RequiredID(String id, boolean active) {
		if (id != null && id.trim().length() == 0) {
			throw new NullPointerException("" + id);
		}
		this.id = id;
		this.name = null;
		this.active = active;
	}

	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
		    return false;
		}
		if (getClass() != obj.getClass()) {
		    return false;
		}
		RequiredID other = (RequiredID) obj;
		if (!id.equals(other.id)) {
		    return false;
		}
		return true;
    }

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public boolean isActive() {
		return active;
	}

	@Override
    public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (id.hashCode());
		return result;
    }

    @Override
    public String toString() {
		final StringBuffer str = new StringBuffer("Criterion [");
		str.append(id);
		if (name != null) {
		    str.append(", ");
		    str.append("'" + name + "'");
		}
		str.append("]");
		return str.toString();
    }
}