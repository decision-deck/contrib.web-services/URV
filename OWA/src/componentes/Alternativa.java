package componentes;

/**
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

public class Alternativa extends RequiredID{
	
	private boolean fictitious;

    public Alternativa(String id, String name, boolean fictitious, boolean active) {
    	super(id, name, active);
		this.fictitious = fictitious;
    }

    public Alternativa(String id, String name, boolean active) {
    	super(id, name, active);
		this.fictitious = false;
    }
    
    public Alternativa(String id, boolean active){
    	super(id, active);
		this.fictitious = false;
    }

    @Override
    public boolean equals(Object obj) {
    	
		if (this == obj) {
		    return true;
		}
		if (obj == null) {
		    return false;
		}
		if (getClass() != obj.getClass()) {
		    return false;
		}
		if (!this.getId().equals(((Alternativa)obj).getId())) {
		    return false;
		}
		return true;
    }

    public boolean isFictitious() {
    	return fictitious;
    }
    
    @Override
    public String toString() {
		final StringBuffer str = new StringBuffer("Alternative");
		str.append(" [");
		str.append(this.getId());
		if (fictitious) {
		    str.append("-FICTITIOUS");
		}
		if (this.getName() != null) {
		    str.append(", '" + this.getName() + "'");
		}
		if (this.isActive()) {
		    str.append("-ACTIVE");
		}
		str.append("]");
		return str.toString();
    }
}
